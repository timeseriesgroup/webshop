package nl.timeseries.training.service;

import java.util.Map;
import java.util.UUID;

import nl.timeseries.training.domain.Basket;
import nl.timeseries.training.domain.Product;

public interface BasketService {

	Basket createBasket();

	Basket get(UUID id);

	void updateProductCount(UUID id, UUID productId, int count);

	void deleteProductCount(UUID id, UUID productId);

	double calculateSubtotal(Basket basket, Product product);

	double calculateTotal(Basket basket, Map<UUID, Product> products);
}
