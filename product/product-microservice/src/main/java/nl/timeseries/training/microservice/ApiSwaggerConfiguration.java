package nl.timeseries.training.microservice;

import io.swagger.annotations.Api;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import springfox.documentation.builders.ApiInfoBuilder;
import springfox.documentation.builders.RequestHandlerSelectors;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import static java.util.Collections.singleton;

@Configuration
@EnableSwagger2
public class ApiSwaggerConfiguration {

	@Bean
	public Docket apiDocket() {
		return new Docket(DocumentationType.SWAGGER_2)
				.apiInfo(apiInfo())
				.consumes(singleton("application/json"))
				.produces(singleton("application/json"))
				.select()
				.apis(RequestHandlerSelectors.withClassAnnotation(Api.class))
				.build();
	}

	private ApiInfo apiInfo() {
		return new ApiInfoBuilder()
				.title("ProductApplication")
				.description("ProductApplication")
				.contact(new Contact("TimeSeries", "http://www.timeseries.nl", "info@timeseries.nl"))
				.version("1.0")
				.build();
	}
}
