package nl.timeseries.training;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.zuul.EnableZuulProxy;

@EnableZuulProxy
@EnableEurekaClient
@SpringBootApplication
public class WebshopApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebshopApiApplication.class, args);
	}
}
