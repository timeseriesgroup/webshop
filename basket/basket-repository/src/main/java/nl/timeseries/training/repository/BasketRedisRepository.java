package nl.timeseries.training.repository;

import org.springframework.data.repository.CrudRepository;

public interface BasketRedisRepository extends CrudRepository<BasketEntity, String> {

}
