package nl.timeseries.training.dto;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "BasketEntry", description = "Describes one basket entry, containing a product,"
		+ " the number of times it's in the basket and the price subtotal.")
public class BasketEntryDto {

	@ApiModelProperty("The product in the basket")
	private ProductDto product;

	@ApiModelProperty(value = "The number of times the product is in the basket",
			example = "3")
	private int count;

	@ApiModelProperty(value = "The subtotal of the count times the product's price",
			example = "60.0")
	private double subTotal;

	public ProductDto getProduct() {
		return product;
	}

	// Getters and setters below


	public void setProduct(ProductDto product) {
		this.product = product;
	}

	public int getCount() {
		return count;
	}

	public void setCount(int count) {
		this.count = count;
	}

	public double getSubTotal() {
		return subTotal;
	}

	public void setSubTotal(double subTotal) {
		this.subTotal = subTotal;
	}
}
