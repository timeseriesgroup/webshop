package nl.timeseries.training.microservice;

import java.util.Map;
import java.util.UUID;
import javax.annotation.PostConstruct;

import nl.timeseries.training.repository.ProductEntity;
import nl.timeseries.training.repository.ProductMongoRepository;
import org.springframework.context.annotation.Configuration;

import static java.util.Collections.emptyMap;
import static java.util.Collections.singletonMap;

@Configuration
public class MockProductsConfiguration {

	private final ProductMongoRepository repository;

	public MockProductsConfiguration(ProductMongoRepository repository) {
		this.repository = repository;
	}

	@PostConstruct
	public void populate() {
		createProductIfAbsent(
				UUID.fromString("ed924764-94aa-4961-bf65-af707f2d7dd4"),
				"Cookie Jar",
				"Keep your delicious cookies deliciously good.",
				"images/cookie_jar_1.jpeg",
				20.0,
				singletonMap("print", "chocolate chip"));
		createProductIfAbsent(
				UUID.fromString("0b05881e-7351-4a64-8d76-0a4452dd3935"),
				"Cookie Jar",
				"Keep your delicious cookies deliciously good.",
				"images/cookie_jar_2.jpeg",
				20.0,
				singletonMap("print", "nuts and berries"));
		createProductIfAbsent(
				UUID.fromString("3c660a4e-eb95-460f-ac89-7088ad1e2615"),
				"Culinary Cutlery Set",
				"Stick em with the pointy end",
				"images/culinary_cutlery_set.jpeg",
				39.99,
				singletonMap("pieces", "16"));
		createProductIfAbsent(
				UUID.fromString("3c3083ca-531e-4e6e-85ee-c11712e91027"),
				"Toy Truck",
				"Plastic toy truck.",
				"images/toy_truck.jpeg",
				14.95,
				emptyMap());
		createProductIfAbsent(
				UUID.fromString("db7f5b24-a310-4e18-9691-a961d432ce48"),
				"Covenant Broom",
				"Good for sweeping.",
				"images/covenant_broom.jpeg",
				9.99,
				singletonMap("material", "nylon"));
	}

	private void createProductIfAbsent(
			UUID id,
			String name,
			String description,
			String image,
			double price,
			Map<String, String> properties) {
		if (repository.findOne(id) == null) {
			ProductEntity entity = new ProductEntity();
			entity.setId(id);
			entity.setName(name);
			entity.setDescription(description);
			entity.setImage(image);
			entity.setPrice(price);
			entity.setProperties(properties);
			repository.save(entity);
		}
	}

}