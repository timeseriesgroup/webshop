package nl.timeseries.training.microservice;

import java.util.UUID;
import javax.servlet.http.HttpServletResponse;

import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;

@ControllerAdvice
public class NodeHeaderControllerAdvice {

	private static final String MY_UUID = UUID.randomUUID().toString();

	@ModelAttribute
	public void setNodeUuidResponseHeader(HttpServletResponse response) {
		response.setHeader("node-uuid", MY_UUID);
	}
}
