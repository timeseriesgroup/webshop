package nl.timeseries.training.repository;

import java.util.UUID;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ProductMongoRepository extends MongoRepository<ProductEntity, UUID> {

}
