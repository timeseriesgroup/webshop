package nl.timeseries.training.service;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import nl.timeseries.training.domain.Basket;
import nl.timeseries.training.domain.Product;
import nl.timeseries.training.repository.BasketEntity;
import nl.timeseries.training.repository.BasketRedisRepository;
import org.springframework.data.redis.core.PartialUpdate;
import org.springframework.data.redis.core.RedisKeyValueTemplate;
import org.springframework.stereotype.Service;

@Service
public class RedisBasketService implements BasketService {

	private final BasketRedisRepository repository;
	private final RedisKeyValueTemplate template;

	public RedisBasketService(
			BasketRedisRepository repository,
			RedisKeyValueTemplate template) {
		this.repository = repository;
		this.template = template;
	}

	@Override
	public Basket createBasket() {
		BasketEntity entity = new BasketEntity();
		entity.setId(UUID.randomUUID().toString());
		return convert(repository.save(entity));
	}

	@Override
	public Basket get(UUID id) {
		return Optional.ofNullable(repository.findOne(id.toString()))
				.map(this::convert)
				.orElse(null);
	}

	@Override
	public void updateProductCount(UUID id, UUID productId, int count) {
		PartialUpdate<BasketEntity> partialUpdate =
				new PartialUpdate<>(id.toString(), BasketEntity.class)
						.set("productCounts.[" + productId.toString() + "]", count);
		template.update(partialUpdate);
	}

	@Override
	public void deleteProductCount(UUID id, UUID productId) {
		PartialUpdate<BasketEntity> partialUpdate =
				new PartialUpdate<>(id.toString(), BasketEntity.class)
						.del("productCounts.[" + productId.toString() + "]");
		template.update(partialUpdate);
	}


	@Override
	public double calculateSubtotal(Basket basket, Product product) {
		return basket.getProductCount(product.getId()) * product.getPrice();
	}

	@Override
	public double calculateTotal(Basket basket, Map<UUID, Product> products) {
		return products.values().stream()
				.mapToDouble(product -> calculateSubtotal(basket, product))
				.sum();
	}

	private Basket convert(BasketEntity entity) {
		Basket basket = new Basket(UUID.fromString(entity.getId()));
		Optional.ofNullable(entity.getProductCounts())
				.ifPresent(productCounts -> productCounts.forEach((uuid, count) -> {
					basket.setProductCount(UUID.fromString(uuid), count);
				}));
		return basket;
	}
}