package nl.timeseries.training.repository;

import java.util.Map;
import java.util.UUID;

import org.springframework.data.annotation.Id;

public class ProductEntity {

	@Id
	private UUID id;

	private String name;
	private String description;
	private String image;
	private double price;

	private Map<String, String> properties;

	// Getters and setters below


	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Map<String, String> getProperties() {
		return properties;
	}

	public void setProperties(Map<String, String> properties) {
		this.properties = properties;
	}
}
