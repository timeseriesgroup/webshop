package nl.timeseries.training.repository;

import java.util.Map;

import org.springframework.data.annotation.Id;
import org.springframework.data.redis.core.RedisHash;

@RedisHash("baskets")
public class BasketEntity {

	@Id
	private String id;
	private Map<String, Integer> productCounts;

	// Getters and setters below


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public Map<String, Integer> getProductCounts() {
		return productCounts;
	}

	public void setProductCounts(Map<String, Integer> productCounts) {
		this.productCounts = productCounts;
	}
}
