package nl.timeseries.training.dto;

import java.util.Map;
import java.util.UUID;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Product", description = "A product sold by the webshop.")
public class ProductDto {

	@ApiModelProperty(value = "A uuid unique to one product",
			position = -1,
			example = "ed924764-94aa-4961-bf65-af707f2d7dd4")
	private UUID id;

	@ApiModelProperty(value = "The name of the product",
			example = "Cookie Jar")
	private String name;

	@ApiModelProperty(value = "A description of the product",
			example = "Keep your delicious cookies deliciously good.")
	private String description;

	@ApiModelProperty(value = "The location to the image on the server",
			example = "images/cookie_jar_1.jpeg")
	private String image;

	@ApiModelProperty(value = "The price for which the product is sold",
			example = "20.00")
	private double price;

	@ApiModelProperty("A map of arbitrary properties that describe the product")
	private Map<String, String> properties;

	// Getters and setters below


	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Map<String, String> getProperties() {
		return properties;
	}

	public void setProperties(Map<String, String> properties) {
		this.properties = properties;
	}
}