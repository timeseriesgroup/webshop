package nl.timeseries.training.domain;

import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

import static java.util.Objects.requireNonNull;

public class Product {

	private final UUID id;

	private String name;
	private String description;
	private String image;
	private double price;

	private final Map<String, String> properties = new HashMap<>();

	public Product(UUID id) {
		this.id = requireNonNull(id);
	}

	// Getters and setters below


	public UUID getId() {
		return id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public Map<String, String> getProperties() {
		return properties;
	}
}