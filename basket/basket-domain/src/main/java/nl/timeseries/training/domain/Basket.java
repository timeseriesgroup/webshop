package nl.timeseries.training.domain;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static java.util.Objects.requireNonNull;

public class Basket {

	private final UUID id;
	private final Map<UUID, Integer> productCounts = new LinkedHashMap<>();

	public Basket(UUID id) {
		this.id = requireNonNull(id);
	}

	public int getProductCount(UUID productId) {
		return Optional.ofNullable(productCounts.get(productId))
				.orElse(0);
	}

	public void setProductCount(UUID productId, int count) {
		productCounts.put(productId, count);
	}

	public void removeProduct(UUID productId) {
		productCounts.remove(productId);
	}

	// Getters and setters below

	public UUID getId() {
		return id;
	}

	public Map<UUID, Integer> getProductCounts() {
		return productCounts;
	}
}
