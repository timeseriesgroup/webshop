package nl.timeseries.training.dto;

import java.util.Map;
import java.util.UUID;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;

@ApiModel(value = "Basket", description = "Describes one shopping basket.")
public class BasketDto {

	@ApiModelProperty(value = "A uuid unique to one basket",
			position = -1,
			example = "759235d1-89d3-49db-a885-52abf32d8f33")
	private UUID id;

	@ApiModelProperty("A map of product uuids with their respective BasketEntryDto")
	private Map<UUID, BasketEntryDto> entries;

	@ApiModelProperty(value = "The total price of the basket's contents",
			example = "120.0")
	private double total;

	// Getters and setters below


	public UUID getId() {
		return id;
	}

	public void setId(UUID id) {
		this.id = id;
	}

	public Map<UUID, BasketEntryDto> getEntries() {
		return entries;
	}

	public void setEntries(Map<UUID, BasketEntryDto> entries) {
		this.entries = entries;
	}

	public double getTotal() {
		return total;
	}

	public void setTotal(double total) {
		this.total = total;
	}
}
