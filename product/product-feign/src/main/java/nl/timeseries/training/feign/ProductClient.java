package nl.timeseries.training.feign;

import java.util.List;
import java.util.UUID;

import nl.timeseries.training.dto.ProductDto;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;

import static org.springframework.http.HttpStatus.CREATED;

@FeignClient(
		name = "product",
		path = "api/1.0/product")
public interface ProductClient {

	@RequestMapping(method = RequestMethod.GET)
	List<ProductDto> getAllProducts();

	@ResponseStatus(CREATED)
	@RequestMapping(method = RequestMethod.POST)
	ProductDto createNewProduct(@RequestBody ProductDto dto);

	@RequestMapping(path = "{id}", method = RequestMethod.GET)
	ProductDto getProduct(@PathVariable("id") UUID id);

	@RequestMapping(path = "{id}", method = RequestMethod.PUT)
	ProductDto updateProduct(@PathVariable("id") UUID id, @RequestBody ProductDto dto);

	@RequestMapping(path = "{id}", method = RequestMethod.DELETE)
	ProductDto deleteProduct(@PathVariable("id") UUID id);
}
