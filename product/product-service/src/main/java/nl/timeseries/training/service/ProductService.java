package nl.timeseries.training.service;

import java.util.List;
import java.util.Map;
import java.util.UUID;

import nl.timeseries.training.domain.Product;

public interface ProductService {

	List<Product> getAll();

	Product createProduct(
			String name,
			String description,
			String image,
			double price,
			Map<String, String> properties);

	Product get(UUID id);

	Product save(Product product);

	Product delete(UUID id);
}