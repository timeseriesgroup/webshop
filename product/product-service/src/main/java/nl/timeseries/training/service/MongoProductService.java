package nl.timeseries.training.service;

import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import nl.timeseries.training.domain.Product;
import nl.timeseries.training.repository.ProductEntity;
import nl.timeseries.training.repository.ProductMongoRepository;
import org.springframework.stereotype.Service;

import static java.util.Collections.emptyMap;
import static java.util.stream.Collectors.toList;

@Service
public class MongoProductService implements ProductService {

	private final ProductMongoRepository repository;

	public MongoProductService(ProductMongoRepository repository) {
		this.repository = repository;
	}

	@Override
	public List<Product> getAll() {
		return repository.findAll().stream()
				.map(this::convert)
				.collect(toList());
	}

	@Override
	public Product createProduct(
			String name,
			String description,
			String image,
			double price,
			Map<String, String> properties) {
		return convert(repository.save(createEntity(
				UUID.randomUUID(),
				name,
				description,
				image,
				price,
				properties == null ? emptyMap() : properties)));
	}

	@Override
	public Product get(UUID id) {
		return convert(repository.findOne(id));
	}

	@Override
	public Product save(Product product) {
		return convert(repository.save(convert(product)));
	}

	@Override
	public Product delete(UUID id) {
		ProductEntity entity = repository.findOne(id);
		if (entity != null) {
			repository.delete(id);
		}
		return convert(entity);
	}

	private ProductEntity createEntity(
			UUID id,
			String name,
			String description,
			String image,
			double price,
			Map<String, String> properties) {
		ProductEntity entity = new ProductEntity();
		entity.setId(id);
		entity.setName(name);
		entity.setDescription(description);
		entity.setImage(image);
		entity.setPrice(price);
		entity.setProperties(properties);
		return entity;
	}

	private ProductEntity convert(Product product) {
		ProductEntity entity = new ProductEntity();
		entity.setId(product.getId());
		entity.setName(product.getName());
		entity.setDescription(product.getDescription());
		entity.setImage(product.getImage());
		entity.setPrice(product.getPrice());
		Optional.ofNullable(product.getProperties())
				.ifPresent(entity::setProperties);
		return entity;
	}

	private Product convert(ProductEntity entity) {
		Product product = new Product(entity.getId());
		product.setName(entity.getName());
		product.setDescription(entity.getDescription());
		product.setImage(entity.getImage());
		product.setPrice(entity.getPrice());
		Optional.ofNullable(entity.getProperties())
				.ifPresent(properties -> product.getProperties().putAll(properties));
		return product;
	}
}
