package nl.timeseries.training.microservice;

import java.util.List;
import java.util.UUID;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import nl.timeseries.training.domain.Product;
import nl.timeseries.training.dto.ProductDto;
import nl.timeseries.training.service.ProductService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpStatus.CREATED;

@Api
@RestController
@RequestMapping("api/1.0/product")
public class ProductController {

	private final ProductService productService;

	public ProductController(ProductService productService) {
		this.productService = productService;
	}

	@GetMapping
	@ApiOperation("Return a list of all known products.")
	public List<ProductDto> getAllProducts() {
		return productService.getAll().stream()
				.map(this::convert)
				.collect(toList());
	}

	@PostMapping
	@ResponseStatus(CREATED)
	@ApiOperation("Create a new product.")
	public ProductDto createNewProduct(
			@ApiParam("The product to create. Note that the id field can be omitted, since a new random UUID will be generated instead.")
			@RequestBody ProductDto dto) {
		return convert(productService.createProduct(
				dto.getName(),
				dto.getDescription(),
				dto.getImage(),
				dto.getPrice(),
				dto.getProperties()));
	}

	@GetMapping("{id}")
	@ApiOperation("Get the product with the given uuid.")
	public ProductDto getProduct(
			@ApiParam("A uuid unique to one product.")
			@PathVariable UUID id) {
		return convert(productService.get(id));
	}

	@PutMapping("{id}")
	@ApiOperation("Update the product with the given uuid to match the given product.")
	public ProductDto updateProduct(
			@ApiParam("A uuid unique to one product.")
			@PathVariable UUID id,
			@ApiParam("The new product. Note that the id field will be ignored"
					+ " and that missing values will be removed from the existing product.")
			@RequestBody ProductDto dto) {
		Product product = productService.get(id);
		product.setName(dto.getName());
		product.setDescription(dto.getDescription());
		product.setImage(dto.getImage());
		product.setPrice(dto.getPrice());
		product.getProperties().clear();
		if (dto.getProperties() != null) {
			product.getProperties().putAll(dto.getProperties());
		}
		return convert(productService.save(product));
	}

	@DeleteMapping("{id}")
	@ApiOperation(value = "Delete the product with the given uuid.",
			notes = "All deletes are final and can not be undone!")
	public ProductDto deleteProduct(
			@ApiParam("A uuid unique to one product.")
			@PathVariable UUID id) {
		return convert(productService.delete(id));
	}

	private ProductDto convert(Product product) {
		ProductDto dto = new ProductDto();
		dto.setId(product.getId());
		dto.setName(product.getName());
		dto.setDescription(product.getDescription());
		dto.setImage(product.getImage());
		dto.setPrice(product.getPrice());
		dto.setProperties(product.getProperties());
		return dto;
	}
}
