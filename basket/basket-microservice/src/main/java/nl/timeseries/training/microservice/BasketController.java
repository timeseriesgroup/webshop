package nl.timeseries.training.microservice;

import java.util.LinkedHashMap;
import java.util.Optional;
import java.util.UUID;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import nl.timeseries.training.domain.Basket;
import nl.timeseries.training.domain.Product;
import nl.timeseries.training.dto.BasketDto;
import nl.timeseries.training.dto.BasketEntryDto;
import nl.timeseries.training.dto.ProductDto;
import nl.timeseries.training.feign.ProductClient;
import nl.timeseries.training.service.BasketService;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Api
@RestController
@RequestMapping("api/1.0/basket")
public class BasketController {

	private final BasketService basketService;
	private final ProductClient productClient;

	public BasketController(BasketService basketService, ProductClient productClient) {
		this.basketService = basketService;
		this.productClient = productClient;
	}

	@PostMapping
	@ResponseStatus(CREATED)
	@ApiOperation("Create a new basket empty and return it.")
	public BasketDto createNewBasket() {
		return convert(basketService.createBasket());
	}

	@GetMapping("{id}")
	@ApiOperation(value = "Get the basket with the given uuid.",
			notes = "Baskets are held in memory, so they can occasionally disappear.")
	public BasketDto getBasket(
			@ApiParam("A uuid unique to one basket.")
			@PathVariable UUID id) {
		Basket basket = Optional.ofNullable(basketService.get(id))
				.orElseThrow(() -> new BasketNotFoundException("Unknown basket " + id));
		return convert(basket);
	}

	@GetMapping("{id}/product/{productId}")
	@ApiOperation(value = "Get the number items of the given product are in the given basket.",
			notes = "Baskets are held in memory, so they can occasionally disappear.")
	public int getEntry(
			@ApiParam("A uuid unique to one basket.")
			@PathVariable UUID id,
			@ApiParam("A uuid unique to one product.")
			@PathVariable UUID productId) {
		Basket basket = Optional.ofNullable(basketService.get(id))
				.orElseThrow(() -> new BasketNotFoundException("Unknown basket " + id));
		return basket.getProductCount(productId);
	}

	@PutMapping("{id}/product/{productId}")
	@ApiOperation(value = "Set the number the given product that are in in the given basket.",
			notes = "Baskets are held in memory, so they can occasionally disappear.")
	public void updateEntry(
			@ApiParam("A uuid unique to one basket.")
			@PathVariable UUID id,
			@ApiParam("A uuid unique to one product.")
			@PathVariable UUID productId,
			@ApiParam("The number of the given product that are in the basket.")
			@RequestBody int count) {
		basketService.updateProductCount(id, productId, count);
	}

	@DeleteMapping("{id}/product/{productId}")
	@ApiOperation(value = "Remove all items of the given product from the given basket.",
			notes = "Baskets are held in memory, so they can occasionally disappear.")
	public void deleteEntry(
			@ApiParam("A uuid unique to one basket.")
			@PathVariable UUID id,
			@ApiParam("A uuid unique to one product.")
			@PathVariable UUID productId) {
		basketService.deleteProductCount(id, productId);
	}

	private BasketDto convert(Basket basket) {
		LinkedHashMap<UUID, Product> products = new LinkedHashMap<>();
		LinkedHashMap<UUID, BasketEntryDto> entries = new LinkedHashMap<>();
		basket.getProductCounts().forEach((productId, count) -> {
			ProductDto productDto = productClient.getProduct(productId);
			if (productDto != null) {
				Product product = convert(productDto);
				products.put(productId, product);

				BasketEntryDto dto = new BasketEntryDto();
				dto.setProduct(productDto);
				dto.setCount(count);
				dto.setSubTotal(basketService.calculateSubtotal(basket, product));
				entries.put(productId, dto);
			}
		});

		BasketDto dto = new BasketDto();
		dto.setId(basket.getId());
		dto.setEntries(entries);
		dto.setTotal(basketService.calculateTotal(basket, products));
		return dto;
	}

	private Product convert(ProductDto dto) {
		Product product = new Product(dto.getId());
		product.setName(dto.getName());
		product.setDescription(dto.getDescription());
		product.setImage(dto.getImage());
		product.setPrice(dto.getPrice());
		product.getProperties().putAll(dto.getProperties());
		return product;
	}

	@ResponseStatus(NOT_FOUND)
	public static class BasketNotFoundException extends RuntimeException {

		public BasketNotFoundException(String message) {
			super(message);
		}
	}
}